const express=require('express')
const cors=require('cors')
const router=require('./router/index')

const app=express()

app.listen('2306',()=>{
    console.log('端口已成功开启端口号是http://localhost:2306');
    
})
app.use(express.json())
app.use(express.urlencoded())
app.use(express.static('./public'))
app.use(cors())
app.use(router)
app.use((err,req,res,next)=>{
    res.send(err)
})
app.use((req,res,next)=>{
    res.status(404).send('<h1>页面找不到了</h1>')
})