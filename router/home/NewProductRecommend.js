const express=require('express')
const router=express.Router()
const data=require('./data/NewProductRecommend')
let detail=[]
router.get('/home/newProductRecommend',(req,res)=>{
    res.send({code:200,message:'请求成功',data:{ newImg:'https://php-b2c.likeshop.cn/uploads/images/20210312141626509a86747.png',data:data}})
})
router.get('/home/newProductRecommend/detail',(req,res)=>{
    const {id} =req.query
    detail=data.find(item=>item.id==id)
    if(!id){
        res.send('id是必须传递的')
    }else{
        res.send({code:200,message:'请求成功',data:{code:200,data:detail}})
    }
})
module.exports=router