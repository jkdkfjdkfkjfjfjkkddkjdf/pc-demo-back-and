const HotSelling =require('./HotSelling')
const NewProductRecommend=require('./NewProductRecommend')
const GoodThingsPreferred=require('./GoodThingsPreferred')
const data=[
    ...HotSelling,
    ...NewProductRecommend,
    ...GoodThingsPreferred
]
module.exports=data