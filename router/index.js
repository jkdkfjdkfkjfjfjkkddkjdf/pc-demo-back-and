const express = require('express')
const router=express.Router()
const home=require('./home/index')
const Login=require('./Login/index')
router.use(home)
router.use(Login)
module.exports=router