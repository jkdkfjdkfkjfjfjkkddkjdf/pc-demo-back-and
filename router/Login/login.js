const express = require('express');
const router=express.Router()
const jwt=require('jsonwebtoken')
const loginData=require('./data/index');
const data = require('./data/index');
let captcha;
//账号密码登录接口
router.post('/login',(req,res)=>{
    const {username,password} =req.body
   if(!username || !password){
      res.send({code:200,data:{code:300,message:'账号和密码是必传参数'}})
   }else{
     let loginFind= loginData.find(item=>item.username==username && item.password==password)
     const token=jwt.sign({username},'zwh',{expiresIn:'24h'})
     if(loginFind){
      res.send({code:200,data:{code:200,message:'登录成功',token,name:loginFind.name}})
     loginFind.token=token
     }else{
      res.send({code:200,data:{code:400,message:'账号或密码错误'}})
     }
   }
  })

  //验证码登录
  //发送验证码
  router.post('/user/sendCaptcha',(req,res)=>{
   let min=100000
   let max=999999
    captcha=Math.floor(Math.random()*(max-min+1)+min)
   res.send({code:200,data:{code:200,message:captcha}})
  })
  //验证登录
  router.post('/user/verifyCaptcha',(req,res)=>{
    const {tel,yzm}=req.body
    const token=jwt.sign({tel},'zwh',{expiresIn:'24h'})
    console.log()
    if(!tel){
     
      //电话号码
      res.send({code:200,data:{code:300,message:'电话号码是必传参数'}})
      //验证验证码
    }else if(!yzm){
     res.send({code:200,data:{code:400,message:'验证码是必传参数'}})
    }
    if(yzm===captcha){
      res.send({code:200,data:{code:200,message:'登录成功',token}})
    }else{
      res.send({code:200,data:{code:500,message:'验证码错误'}})
    }
  })
module.exports=router
