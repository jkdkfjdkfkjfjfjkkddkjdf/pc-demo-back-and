const express = require('express')
const router=express.Router()
const data=require('./data/index')
const detail=require('../home/data/detail')
let active=false
// 加入购物车
router.post('/cart/add',(req,res)=>{
    const {username,cart}=req.body
    if(!username || !cart){
        res.send({code:200,data:{message:'用户名和添加的购物车是必传参数'}})
    }else{
       let cartData=data.find(item=>item.username==username)
       if(cartData){
         cartData.cart=cart
         res.send({code:200,data:{code:200,message:'加入购物车成功'}})
       }else{
        res.send({code:200,data:{message:'用户名错误'}})
       }
    }
})
//查询所有的购物车数据
router.post('/cart/list',(req,res)=>{
const {username}=req.body
if(!username){
  res.send({code:200,data:{code:300,message:'请登录'}})
}
let cartInfo=data.find(item=>item.username==username)
if(cartInfo){
  res.send({code:200,data:{code:200,data:cartInfo.cart}})
}
})


//删除购物车的数据接口
router.post('/cart/delet',(req,res)=>{
  const {username,id}=req.body
  if(!username || !id){
    res.send({code:200,data:{message:'用户名和添加的购物车是必传参数'}})
}else{
   let cartData=data.find(item=>item.username==username)
   if(cartData){
   cartData.cart=cartData.cart.filter(item=>item.id!=id)
     res.send({code:200,data:{code:200,message:'删除成功',data:cartData.cart}})
   }else{
    res.send({code:200,data:{message:'用户名错误'}})
   }
}
})

//收藏商品
router.post('/cart/addFavorites', (req, res) => {
  const { username, id } = req.body;
  if (!username) {
    return res.send({ code: 200, data: { code: 300, message: '用户名是必传项' } });
  } else if (!id) {
    return res.send({ code: 200, data: { code: 400, message: '请传入要收藏的商品' } });
  }
 
  let cartData = data.find(item => item.username == username);
  if (!cartData) {
    return res.send({ code: 200, data: { message: '用户名错误' } });
  }
 
  let favoriteItem = detail.find(item => item.id == id);
  if (!favoriteItem) {
    return res.send({ code: 200, data: { code: 500, message: '商品未找到' } });
  }
 
  // 检查商品是否已在收藏夹中
  const isFavorite = cartData.Favorites.some(item => item.id == id);
  if (isFavorite) {
    // 删除收藏夹中的商品
    const index = cartData.Favorites.findIndex(item => item.id == id);
    cartData.Favorites.splice(index, 1);
    active=false
    res.send({ code: 200, data: { code: 200, message: active==true?'已收藏':'已取消收藏' } });
  } else {
    // 添加到收藏夹
    cartData.Favorites.push(favoriteItem);
    active=true
    res.send({ code: 200, data: { code: 200, message: active==true?'已收藏':'已取消收藏'} });
  }
});
//获取手藏列表
router.post('/cart/getFavorites',(req, res)=>{
  const {username}=req.body
  if(!username){
    res.send({code:200,data:{code:300,message:'用户名是必传项'}})
  }else{
    let cartData=data.find(item=>item.username==username)

    res.send({code:200,data:{code:200,message:'请求成功',data:cartData.Favorites}})
  }
}) 
router.post('/cart/exitFavorites',(req,res)=>{
  const {username,id}=req.body
  if(!username){
    res.send({code:200,data:{code:300,message:'用户名是必传项'}})
  }else{
    let cartData=data.find(item=>item.username==username)
    if(cartData){
      active=false
       cartData.Favorites=cartData.Favorites.filter(item=>item.id!=id)
       res.send({code:200,data:{code:200,message:active==true?'已收藏':'已取消收藏',data:cartData.Favorites}})
    }
  }
})
module.exports=router