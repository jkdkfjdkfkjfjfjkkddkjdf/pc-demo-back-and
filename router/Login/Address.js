const express =require('express')
const login = require('./data')
const data = require('./data')
const router=express.Router()
router.post('/address/addAddress',(req,res)=>{
    let count=0
    const {username,data}=req.body
    if(!username){
        res.send({code:200,data:{code:300,message:'用户名是必传项'}})
    }else{
        let addressData=login.find(item=>item.username==username)
       let active= addressData.address.find(item=>item.contact==data.contact && item.contactPhone==data.contactPhone && item.fullAddress==data.fullAddress)
       if(active){
        res.send({code:200,data:{code:400,message:"已经有这个地址了！！！"}})
        return;
       }
       let check=addressData.address.find(item=>item.setDefault==true)
      if(check){
        if(data.setDefault==true){
            res.send({code:200,data:{code:500,message:'已经有默认地址了！！！'}})
            return;
        }
      }
        if(addressData){
          addressData.address.push(data)
          res.send({code:200,data:{code:200,message:'添加成功!!!'}})
        }
    }
})
//获取所有地址
router.post('/address/getAddress',(req,res)=>{
   let min=0
   let max=100
    const {username}=req.body
    if(!username){
        res.send({code:200,data:{code:300,message:'用户名是必传项'}})
    }else{
        let addressData=login.find(item=>item.username==username)
        if(addressData){
            res.send({code:200,data:{code:200,message:'请求成功',data:addressData.address}})
        }
    }
})
//设为默认和取消设为默认
router.post('/address/setDefault',(req,res)=>{
    const {username,name}=req.body
    if(!username){
        res.send({code:200,data:{code:300,message:'用户名是必传项'}})
    }else{
        let addressData=login.find(item=>item.username==username)
        if(addressData){
            let addressName=addressData.address.find(item=>item.contact==name)
            if(addressName){
               if(addressName.setDefault==true){
                addressName.setDefault=false
               }else{
                addressName.setDefault=true
               }

               let countOfSetDefaults = addressData.address.filter(item => item.setDefault == true).length;
               if (countOfSetDefaults > 1) {
                   // 如果超过一个 setDefault 为 true，则处理错误（例如，重置或返回错误消息）
                   // 这里可以根据需求选择如何处理，比如重置所有 setDefault 或返回错误
                   // 示例：重置所有 setDefault 为 false，只保留当前操作的为 true
                   addressData.address.forEach(item => {
                       item.setDefault = false;
                   });
                   addressName.setDefault = true; // 重新设置当前操作的为 true

                   // 或者直接返回错误
                   res.send({ code: 200, data: { code: 400, message: '不能同时有多个默认地址' } });
                   return; // 结束函数执行

               }
               res.send({code:200,data:{code:200,message:addressName.setDefault?'设置成功！':'取消设置成功',data:addressName.setDefault}})
            }
        }
    }
})

//删除地址
router.post('/address/deleteAddress',(req,res)=>{
    const {username,name} = req.body
    if(!username){
        res.send({code:200,data:{code:300,message:'用户名是必传项'}})
    }else{
        let addressData=login.find(item=>item.username==username)
        if(addressData){
             addressData.address=addressData.address.filter(item=>item.contact!=name)
            res.send({code:200,data:{code:200,message:'删除成功',data:addressData.address}})
        }
    }
})
module.exports=router